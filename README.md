# shinden-mycss

## Spis treści
- [shinden-mycss](#shinden-mycss)
  - [Spis treści](#spis-treści)
  - [Informacje](#informacje)
  - [Użycie](#użycie)
    - [Przykłady](#przykłady)
  - [Użyte zasoby](#użyte-zasoby)

## Informacje
Prosta zmiana wyglądu listy mangi/anime/novelek dla nowej listy na shinden.pl przez użytkownika zsuatem. Jak chcesz zobaczyć aktualną wersję zawsze jest ona dostępna na mojej liście.
**Tło jest już zawarte w liście nie ma potrzeby ustawiać go osobno!**

## Użycie
**UWAGA!!!**
**Pamiętaj, że wybrana przez Ciebie wersja nie zaktualizuje się automatycznie do nowszej! Taką zminę trzeba bedze wykonać ręcznie!**

**Na początku należy zresetować cały wygląd listy i ustawić szablon jako "Podstawowy" inaczej mogą występować błędy!**
Aby dodać ten wygląd listy na shindenie należy skopiować "Commit SHA" dla wybranej wersji listy (lista wersji tutaj polecana zawsze najnowsza) https://gitlab.com/zushindeny/customcss-lista/-/commits/master/list/style.css.
Następnie w linku `https://gitlab.com/zushindeny/customcss-lista/aaaaaaaaaaaa/list/style.css` podmieniamy `aaaaaaaaaaaa` na skopiowany "Commit SHA" dla wybranej wersji i tak przygotowany link wklejamy w ustawianiach listy "Własny CSS"


### Przykłady
Kopiujemy SHA np. `d1a9ef56b3a298a12bb701e9b4bf2ee6ba59486e` teraz zmieniamy ciąg liter `a` na to co skopiowaliśmy i otrzymujemy gotowy link `https://glcdn.rawgit.org/zushindeny/customcss-lista/d1a9ef56b3a298a12bb701e9b4bf2ee6ba59486e/list/style.css`


## Użyte zasoby
CDN: https://rawgit.org
Tło: https://i.imgur.com/poQXO58.jpg